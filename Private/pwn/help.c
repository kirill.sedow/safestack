#include <stdio.h>
#include <unistd.h>
#include <sys/fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <setjmp.h>
#include <signal.h>
#include <sys/mman.h>
#include <time.h>

void init() {
	srand(time(NULL));
}

int getValue()
{
	return rand() % 10 + 2;
}