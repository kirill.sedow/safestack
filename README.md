# Challenge Description

This is an advanced CTF (Capture The Flag)-style challenge that requires a series of steps to exploit successfully.

## Legend
Welcome to the Safe Steak House, renowned for its exquisite steaks but at a steep price - about 1 billion dollars per steak! You find yourself in this restaurant with just 10 dollars in your pocket, yet yearning for that steak.

Intrigued, you discover a small casino inside the restaurant offering a Black-Jack-like card game. With the goal of winning enough to afford at least one steak, you decide to try your luck at the game.

Should you succeed, you can indulge in reading a fresh newspaper, catching up on the latest world events while your billion-dollar steak is being prepared. 

After savoring the steak, you're welcome to leave feedback in the restaurant's guest book before departing.

Get started by downloading the `vuln` binary and dive into the game!

## Magic Number 21 - Game Rules

### Objective
"Magic Number 21" is a game played in rounds against an opponent, Alice. The goal is to accumulate a sum of numbers greater than Alice's sum while keeping the total under 21.

### Gameplay
- **Starting the Round**: Each round begins with you holding a randomly chosen number.
- **Accumulation Decision**: Decide whether to continue adding more numbers to your stack. If you choose to continue, a random number (value between 2 and 11) is added to your sum.
- **Turn Passing**: If you choose to stop accumulating numbers, the turn passes to Alice, who then makes her accumulation decisions.
- **Winning a Round**: Achieve a sum greater than Alice's without exceeding 21 to win the round. If your sum exceeds 21, you lose the round. The same rules apply to Alice.
- **Instant Win**: If you accumulate exactly 21, you win the round immediately. The same applies to Alice.
- **Comparison**: If both players stop without exceeding 21, the sums are compared. The player with the higher sum wins the round.
- **Credits**: Your credits are either doubled or halved based on whether you win or lose the round.


# Project Structure

## Public Folder
This folder contains files that are visible to an exploiter:

- `names.txt`: Contains the names of the persons for the game.
- `newspaper.txt`: Includes newspaper entries. Feel free to add more entries.
- `README.md`: Provides a readme for the challenger. It specifies the target system library version and hints that the provided files do not contain useful information for obtaining the flag.
- `vuln.c`: The source code of the vulnerable program.
- `vuln`: The compiled version of `vuln.c`.

## Private Folder
Contains files that need to be deployed on the server side.

## Helper Files
Includes:

- `Dockerfile`: Used to build a binary with the SafeStack flag enabled.
- `get_flag`: A dummy file for the Docker container.

## Presentation
Contains slides that describe the project and the solution in detail.

## Solution
This folder houses a Python script that serves as an exploit.

# Exploit

We encourage you to challenge yourself by attempting to solve the puzzle or identify vulnerabilities on your own first. 

For an in-depth explanation of the vulnerabilities and the exploit, refer to the `exploit.md` file in the "Solution" folder or consult the presentation slides for more insights.